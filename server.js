//noinspection JSHint
/**
 * Created by vladymyr on 1/26/14.
 */
var http = require('http'),
    director = require('director'),
    fs = require('fs'),
    WebSocketServer = new require('ws'),
    SpaceShip = require('./Position').SpaceShip,
    ta = require('./PositionCalculator');

var server = new http.Server(),
    router = new director.http.Router({
        '/:fileName': {
        get: function(fileName){
            "use strict";
                sendFile(__dirname + '/public/' + fileName, this.res);
            }
        }
    });

server.on('request', function(req, resp){
    "use strict";
    router.dispatch(req, resp, function(err) {
        if(!err) return;
        resp.writeHead(404);
        resp.end('Not Found');
    });
});

server.listen(8080, '127.0.0.1');


var sendFile = function(fileName, res) {
    var fileStraem = fs.createReadStream(fileName);
    fileStraem.on('open', function () {
        fileStraem.pipe(res);
    });
    fileStraem.on('error', function(error) {
        res.statusCode = 500;
        res.end("Server error " + error);
    });
};

var clients = {},
    ships = {},
    bullets = {};

var webSocketServer = new WebSocketServer.Server({port: 8081});


webSocketServer.on('connection', function(ws) {
    var id = Math.random(),
    	event,
        shipPosition = ships[id] = new SpaceShip();

    clients[id] = ws;

    ws.on('message', function(message) {

    	event = JSON.parse(message);
    	switch (event.EventName)
    	{
    		case "KeyDown":
                switch (event.KeyCode)
                {
                    case 37:
                        //right
                        shipPosition.right();
                        break;
                    case 38:
                        //top
                        shipPosition.boost();
                        break;
                    case 39:
                        //left
                        shipPosition.left();
                        break;
                    case 40:
                        //down
                        shipPosition.back();
                        break;
                    case 32 :
                        var bullet = shipPosition.fire();
                        if(!!bullet) bullets[Math.random()] = bullet;
                        else console.log("no bullet in the gun!");
                    break;
                }
            break;
            case "PageSizeChanged":
                ta.setAreal(event.Height, event.Width);

            break;
		}
    });
    ws.on('close', function() {
        delete clients[id];
        delete ships[id];
    });

    shipPosition.onDead(function () {
        delete ships[id];
        delete clients[id];
    });
});

ta.initActionQueue(clients, ships, bullets, 30);
