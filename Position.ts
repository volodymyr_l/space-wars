///<reference path="node.d.ts"/>

class SpacePosition {
    pos : number[];
    speed: number[];
    boostValue: number;

    constructor( x: number, y: number, a: number) {
        this.pos = [y, x, a];
        this.speed = [0, 0, 0];
        this.boostValue = 3;
    }
    move ()
    {
        this.pos[0] -= this.speed[0];
        this.pos[1] += this.speed[1];
        this.pos[2] += this.speed[2];
    }
    boost ()
    {
        this.speed[0] += Math.cos(this.pos[2] * Math.PI / 180) * this.boostValue;
        if(this.speed[0] > 5) this.speed[0] = 5;
        else if (this.speed[0] < -5) this.speed[0] = -5;

        this.speed[1] += Math.sin(this.pos[2] * Math.PI / 180) * this.boostValue;
        if(this.speed[1] > 5) this.speed[1] = 5;
        else if(this.speed[1] < -5) this.speed[1] = -5;
    }
    getPositionVector() {
        return this.pos;
    }
}



class Area extends SpacePosition {
    gabarite: number[];

    constructor(x:number, y:number, a:number,
                w:number, h:number)
    {
        super(x, y, a);
        this.gabarite = [w, h];
    }

    getCenterPoint() :number[]
    {
        return [
            this.pos[0] + this.gabarite[1] / 2,
            this.pos[1] + this.gabarite[0] / 2
        ];
    }
}


class Util {
    isContain(positionOne : Area, positionTwo: Area) : boolean
    {
        if( positionOne.pos[0] > positionTwo.pos[0] + positionTwo.gabarite[0]) return false;
        else if( positionOne.pos[0] + positionOne.gabarite[0] < positionTwo.pos[0]) return false;
        else if( positionOne.pos[1] > positionTwo.pos[1] + positionTwo.gabarite[1]) return false;
        else if( positionOne.pos[1] + positionOne.gabarite[1] < positionTwo.pos[1]) return false;
        else return true;
    }
}

class SpaceShip extends Area {
    life: number;
    timeToReloding: number;

    private deadHandler : () => void;
    constructor() {
        super(50, 50, 0, 86, 60);
        this.life = 100;
        this.timeToReloding = 0;
    }

    private getBulletStartPosition () : number[]
    {
        var radius = (this.gabarite[0] + this.gabarite[1]) / 2,
            center = this.getCenterPoint();
        return [
            center[0] + radius * Math.sin(this.pos[2] * Math.PI / 180  + Math.PI * 1.5),
            center[1] + radius * Math.cos(this.pos[2] * Math.PI / 180  + Math.PI * 1.5),
            this.pos[2]
        ];
    }

    left()
    {
        this.speed[2] += 1;
    }

    right()
    {
        this.speed[2] -= 1;
    }

    back()
    {
        this.speed = [0, 0, 0];
    }

    fire () : Bullet
    {
        if(this.timeToReloding < 0) return null;
        this.timeToReloding = -10;
        return new Bullet(this.getBulletStartPosition());
    }

    beFired ()
    {
        console.log("ship is on fired!");
        this.life -= 5;
        if(this.life <= 0) this.die();
    }

    getLife ()
    {
        return this.life;
    }

    private die()
    {
        console.log("ship is die!");
        this.deadHandler();
    }


    onDead(dieHandler: () => void) {
        this.deadHandler = dieHandler;
    }

    reloadGun()
    {
        if(this.timeToReloding < 0) this.timeToReloding++;
    }

}


class Bullet extends Area {
    constructor(startPosition:number[]) {

        super( startPosition[1], startPosition[0], startPosition[2], 5, 5);
        this.boostValue = 7;
        this.boost();
    }
}


exports.Position = SpacePosition;
exports.Util = new Util();
exports.SpaceShip = SpaceShip;
exports.Bullet = Bullet;
