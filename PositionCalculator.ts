/**
 * Created by vladymyr on 2/4/14.
 */

///<reference path="node.d.ts"/>

var util = require('./Position').Util;

class PositionCalculator  {
     _Clients: any[];
     _Bullets: Bullet[];
     _Ships: SpaceShip[];
     _FPS: number;
     _Areal: any = { pos: [0, 0], size: [800, 600]};

     constructor(clients:any[], ships:SpaceShip[], bullets:Bullet[], fps:number)
     {
         this._Clients = clients;
         this._Ships = ships;
         this._Bullets = bullets;
         this._FPS = fps;
     }

    public setAreal(width:number, height:number)
    {
        this._Areal.size = [height, width];
    }

    public startGaimCircle() {
      setInterval(() => this.mainGaimCircle(), 1000 / this._FPS);
    }

    public mainGaimCircle ()
    {
        for(var shipId in this._Ships)
        {
            this._Ships[shipId].reloadGun();
        }
        this.firingCalculation();
        this.sendingChanges();
    }

    firingCalculation () {
        var bulletId, shipId;
        for(bulletId in this._Bullets)
        {
            for(shipId in this._Ships)
            {
                if(!util.isContain(this._Ships[shipId], this._Bullets[bulletId])) continue;

                delete this._Bullets[bulletId];
                this._Ships[shipId].beFired();
                break;
            }
        }
    }

    isInScreen(pos: number[])
    {
        return this._Areal.pos[0] + this._Areal.size[0] - 20 < pos[0] ||
            this._Areal.pos[0] > pos[0] ||
            this._Areal.pos[1] + this._Areal.size[1] - 20 < pos[1] ||
            this._Areal.pos[1] > pos[1];
    }

    getNextPositionDict(items: SpacePosition[]) {
        var res = {},
            itemId, item, pos;
        for (itemId in items)
        {
            item = items[itemId];
            item.move();
            pos = item.getPositionVector();
            if(this.isInScreen(pos)) continue;
            res[itemId] = pos;
        }
        return res;
    }

    sendingChanges ()
    {
        var ships = this.getNextPositionDict(this._Ships),
            bulletVectors = this.getNextPositionDict(this._Bullets),
            life = {},
            positionId, clientId, positionJSON;

        for(positionId in this._Ships)
        {
            life[positionId] = this._Ships[positionId].getLife();
        }

        positionJSON = JSON.stringify({'spObj':{ 'ship' : ships, 'bullet': bulletVectors, 'life':life} });

        for(clientId in this._Clients)
        {
            this._Clients[clientId].send(positionJSON);
        }
    }
};

var pc:PositionCalculator;

exports.initActionQueue = function (clients:any[], ships:SpaceShip[], bullets:Bullet[], fps:number) {
    if(pc) return;
    pc = new PositionCalculator(clients, ships, bullets, fps);
    pc.startGaimCircle();
};

exports.setAreal = function (height:number, width:number) {
  if(!pc) return;
  pc.setAreal(width, height);
};
