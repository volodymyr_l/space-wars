/**
 * Created by vladymyr on 1/26/14.
 */

  var X = 1, Y = 0, A = 2;

  //init gameplay
  document.body.style.backgroundColor = '#051035';

  class Space
  {
    shipDom:{ [n:number]: any; }  = {};
    bulletDom:{ [n:number]: any; }  = {};
    spaceObjects:
      {
        ship: {[n:number]: any; };
        bullet: {[n:number]: any; };
        life: {[n:number]: number; };
      } = {ship:{}, bullet:{}, life:{}};

    setSpaceObjects(spaceObjects: any)
    {
      this.spaceObjects = spaceObjects;
    }

    getActiveImage(id, url)
    {
      var img = document.createElement("img");
      img.src = url;
      img.style['position'] = 'absolute';
      img.id = id;
      return img;
    }

    addSheep(id)
    {
      var fragment = document.createDocumentFragment();
      var life = document.createElement("span");
      life.id = id + "life";
      fragment.appendChild(life);

      var imgShip = this.getActiveImage(id, '1.png');

      fragment.appendChild(imgShip);

      this.shipDom[id] = imgShip;
      document.body.appendChild(fragment);
    }

    addBullet(id)
    {
      var imgBullet = this.getActiveImage(id, '2.png');
      this.bulletDom[id] = imgBullet;
      document.body.appendChild(imgBullet);
    }

    setPosition(coord:{ [n:number]:number; }, domNode)
    {
      var rotateValStr = 'rotate(' + coord[A] + 'deg)';

      domNode.style['top'] =  coord[Y] + 'px';
      domNode.style['left'] = coord[X] + 'px';

      domNode.style['transform'] =
      domNode.style['-webkit-transform'] =
      domNode.style['-moz-transform'] = rotateValStr;
    }

    public updateGamePlay()
    {
      var ship = this.spaceObjects.ship,
          bullet = this.spaceObjects.bullet,
          life = this.spaceObjects.life, i;

      for(i in ship)
      {
        if (!(i in this.shipDom)) this.addSheep(i);
        this.setPosition(ship[i], this.shipDom[i])
      }

      for(i in life)
      {
        if (!(i in this.shipDom)) continue;
        var lifeSpan = document.getElementById(i + 'life');
        lifeSpan.innerHTML = life[i];
      }

      for(i in bullet)
      {
        if (!(i in this.bulletDom)) this.addBullet(i);
        this.setPosition(bullet[i], this.bulletDom[i])
      }
    }

    removeItem(items, itemsDom)
    {
      for(var i in itemsDom)
      {
        if (i in items) continue;
        itemsDom[i].parentNode.removeChild(itemsDom[i]);
        delete itemsDom[i];
      }
    }

    public clearGamePlay()
    {
      var ship = this.spaceObjects.ship,
          bullet = this.spaceObjects.bullet;

      this.removeItem(bullet, this.bulletDom);
      this.removeItem(ship, this.shipDom);
    }

    public init()
    {
      setInterval(() => this.updateGamePlay(), 1000 / 25);
      setInterval(() => this.clearGamePlay(), 1000 / 5);

      var socket = new WebSocket("ws://localhost:8081");
      socket.onmessage = (event) =>
      {
        var message = JSON.parse(event.data);
        if ('spObj' in message) {
            this.spaceObjects = message.spObj;
        }
      };

      function getDocHeight()
      {
        var D = window.document;
        return Math.max(
          D.body.scrollHeight, D.documentElement.scrollHeight,
          D.body.offsetHeight, D.documentElement.offsetHeight,
          D.body.clientHeight, D.documentElement.clientHeight
        );
      }

      function getDocWidth()
      {
        var D = window.document;
        return Math.max(
          D.body.scrollWidth, D.documentElement.scrollWidth,
          D.body.offsetWidth, D.documentElement.offsetWidth,
          D.body.clientWidth, D.documentElement.clientWidth
        );
      }

      socket.onopen = function ()
      {
        var validKeys = {32:'',37:'', 38:'', 39:'', 40:''},
            message;

        document.onkeydown = function (eventArg)
        {
          if(eventArg.keyCode in validKeys)
          {
            message = JSON.stringify({"EventName":"KeyDown", "KeyCode":eventArg.keyCode});
            socket.send(message);
            return;
          }
        };

        var currentSize = [0, 0];
        setInterval(function()
        {
          var height = getDocHeight(), width = getDocWidth();
          if(currentSize[Y] == height && currentSize[X] == width) return;
          currentSize[Y] = height; currentSize[X] = width;
          message = JSON.stringify({"EventName":"PageSizeChanged", "Height":height, "Width":width});
          socket.send(message);
        }, 1000);
      }
    }
  }

  var _item = new Space();
  _item.init();
